import { RouterFactory } from "../interfaces/general";
import express, { Request, Response } from "express";
import passport from "passport";
import bcrypt from "bcrypt";
import bodyParser from "body-parser";
import multer from "multer";
import { User, UserRole } from "../models/user.model";
import { Experience } from "../models/experience.model";
import { Project } from "../models/project.model";
import { Feedback } from "../models/feedback.model";
import permissions from "../middleware/checkPermission";
import { storage } from "../libs/utils";
import { body, query } from "express-validator";
import {CacheService} from "../services/CacheService";
import {redisClient} from "../redis_client";

const upload = multer({ storage: storage });
const urlencodedParser = bodyParser.urlencoded({ extended: false });

export const makeUserRouter: RouterFactory = () => {
  const router = express.Router();

  const cacheMiddleware = (req: Request, res: Response, next: any) => {
    const userId = req.params.userId;
    const cacheKey = `cv:${userId}`;

    redisClient.get(cacheKey, (err, data) => {
      if (err) {
        console.error('Error getting data from cache:', err);
        next();
      }

      if (data) {
        res.status(200).json(JSON.parse(data));
      } else {
        next();
      }
    });
  };


  router.post(
    "",
    urlencodedParser,
    upload.single("image"),
    passport.authenticate("jwt", { session: false }),
    [
      body("firstName").isString().isLength({ max: 255 }),
      body("lastName").isString().isLength({ max: 255 }),
      body("title").isString().isLength({ max: 255 }),
      body("summary").isString().isLength({ max: 255 }),
      body("email").isEmail().isLength({ max: 255 }),
      body("password").isString().isLength({ min: 8, max: 255 }),
      body("role").not().exists(),
    ],
    async (req: Request, res: Response) => {
      const currentUser = req.user as User;
      req.log.info('Log message from the route handler');

      if (currentUser.role != UserRole.Admin) {
        return res.status(400).json({
          message: "validation failed you are not admin",
        });
      }

      const { firstName, lastName, title, summary, email, password, role } =
        req.body;

      try {
        const image = req.file ? req.file.filename : null;
        const hashedPassword = await bcrypt.hash(password, 10);

        const user = await User.create({
          firstName,
          lastName,
          title,
          summary,
          email,
          image,
          password: hashedPassword,
          role,
        });
        await CacheService(currentUser.id);

        res.status(201).json({
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          title: user.title,
          summary: user.summary,
          image: user.image,
          email: user.email,
          role: user.role,
        });
      } catch (error) {
        console.error(error);
        return res
          .status(505)
          .json({ error: "Something went wrong on the server." });
      }
    },
  );

  router.get(
    "",
    passport.authenticate("jwt", { session: false }),
    permissions(["Admin"]),
    query("pageSize").notEmpty(),
    query("page").notEmpty(),
    async (req: Request, res: Response) => {
      const currentUser = req.user as User;
      req.log.info('Log message from the route handler');

      if (currentUser.role !== UserRole.Admin) {
        return res.status(400).json({
          message: "Validation failed: You are not an admin.",
        });
      }

      try {
        const pageSize = Number(req.query.pageSize);
        const page = Number(req.query.page);
        if (isNaN(pageSize) || isNaN(page)) {
          return res.status(400).json({
            message:
              "Invalid page size or value. Please provide valid numbers.",
          });
        }

        const users = await User.userPagination(pageSize, page);
        res.status(201).json({
          users,
        });
      } catch (error) {
        console.error(error);
        return res
          .status(500)
          .json({ error: "Something went wrong on the server." });
      }
    },
  );
  router.get("/:id", async (req: Request, res: Response) => {
    req.log.info('Log message from the route handler');
    try {
      const userId = parseInt(req.params.id, 10);

      if (isNaN(userId)) {
        return res.status(400).json({
          error: "Invalid id. The id should be a number.",
        });
      }
      const user = await User.findByPk(userId);

      if (!user) {
        return res.status(404).json({ message: "User not found" });
      }

      res.status(200).json({
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        title: user.title,
        summary: user.summary,
        email: user.email,
        password: user.password,
        role: user.role,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: "Something went wrong." });
    }
  });

  router.put(
    "/:id",
    passport.authenticate("jwt", { session: false }),
    upload.single("image"),
    [
      body("firstName").isString().isLength({ max: 255 }),
      body("lastName").isString().isLength({ max: 255 }),
      body("title").isString().isLength({ max: 255 }),
      body("summary").isString().isLength({ max: 255 }),
      body("email").isEmail().isLength({ max: 255 }),
      body("role").not().exists(),
    ],
    async (req: Request, res: Response) => {
      const currentUser = req.user as User;
      const userId = req.params.id;
      req.log.info('Log message from the route handler');

      try {
        const user = await User.findByPk(userId);
        if (!user) {
          return res.status(404).json({ message: "User not found" });
        }

        if (currentUser.id !== parseInt(userId, 10)) {
          return res.status(403).json({
            message: "Np permission.",
          });
        }

        const { firstName, lastName, title, summary, email, password, role } =
          req.body;

        user.firstName = firstName;
        user.lastName = lastName;
        user.title = title;
        user.summary = summary;
        user.email = email;
        user.role = role;

        if (password) {
          user.password = await bcrypt.hash(password, 10);
        }

        if (req.file) {
          const uniqueSuffix =
            Date.now() + "-" + Math.round(Math.random() * 1e9);
          const ext = req.file.originalname.split(".").pop();
          user.image = `${req.file.fieldname}-${uniqueSuffix}.${ext}`;
        }

        await user.save();
        await CacheService(currentUser.id);

        res.status(200).json({
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          title: user.title,
          summary: user.summary,
          email: user.email,
          role: user.role,
        });
      } catch (error) {
        console.error(error);
        return res.status(500).json({ error: "Something went wrong." });
      }
    },
  );
  router.delete(
    "/:id",
    passport.authenticate("jwt", { session: false }),
    async (req: Request, res: Response) => {
      const currentUser = req.user as User;
      const userId = req.params.id;
      req.log.info('Log message from the route handler');

      const userIdN = parseInt(req.params.id, 10);

      if (isNaN(userIdN)) {
        return res.status(400).json({
          error: "The id should be a number.",
        });
      }

      try {
        const user = await User.findByPk(userId);
        if (!user) {
          return res.status(404).json({ message: "User not found" });
        }

        if (
          currentUser.id !== parseInt(userId, 10) &&
          currentUser.role !== UserRole.Admin
        ) {
          return res.status(403).json({
            message: "Deleting Account is not allowed.",
          });
        }

        await user.destroy();
        await CacheService(currentUser.id);

        return res.sendStatus(204).json({
          message: "Success.",
        });
      } catch (error) {
        console.error(error);
        return res.status(500).json({ error: "Something went wrong." });
      }
    },
  );
  router.get('/:userId/cv', cacheMiddleware, async (req: Request, res: Response) => {
    req.log.info('This is a log message from the route handler');

    try {
      const userId = req.params.userId;
      const cacheKey = `cv:${userId}`;

      const user = await User.findByPk(userId);

      if (!user) {
        return res.status(404).json({ message: 'User not found' });
      }

      const experiences = await Experience.findAll({
        where: { user_id: userId },
      });
      const projects = await Project.findAll({ where: { user_id: userId } });
      const feedbacks = await Feedback.findAll({
        where: { from_user: userId },
        include: [
          {
            model: User,
            as: 'toUser',
            attributes: ['id', 'firstName', 'lastName'],
          },
        ],
      });

      const cv = {
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        title: user.title,
        image: user.image,
        summary: user.summary,
        email: user.email,
        experiences: experiences.map((experience) => ({
          userId: experience.user_id,
          companyName: experience.company_name,
          role: experience.role,
          startDate: experience.startDate,
          endDate: experience.endDate,
          description: experience.description,
        })),
        projects: projects.map((project) => ({
          id: project.id,
          userId: project.user_id,
          image: project.image,
          description: project.description,
        })),
        feedbacks: feedbacks.map((feedback) => ({
          id: feedback.id,
          fromUser: feedback.from_user,
          companyName: feedback.company_name,
          to_user: feedback.to_user,
          context: feedback.content,
        })),
      };

      redisClient.setex(cacheKey, 3600, JSON.stringify(cv));

      res.status(200).json(cv);
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: 'Something went wrong on the server.' });
    }
  });


  return router;
};
