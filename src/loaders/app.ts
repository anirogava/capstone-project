import { loadMiddlewares } from "./middlewares";
import { loadRoutes } from "./routes";
import express, { NextFunction, Request, Response } from "express";
import { loadContext } from "./context";
import { loadModels } from "./models";
import { loadSequelize } from "./sequelize";
import { config } from "../config";
import { loadPassport } from "./passport";
import { Logger } from "../libs/logger";
import requestId from "express-request-id";
import pino from "pino";

declare global {
  namespace Express {
    export interface Request {
      id?: string;
      log?: pino.Logger;
    }
  }
}
export const loadApp = async () => {
  const app = express();
  const sequelize = loadSequelize(config);

  loadModels(sequelize);

  const context = await loadContext();

  app.use(express.json());
  app.use(requestId());

  app.use((req: Request, res: Response, next: NextFunction) => {
    const requestId = req.id;
    req.log = Logger(requestId);

    next();
  });

  loadPassport(app, context);
  loadMiddlewares(app, context);
  loadRoutes(app, context);

  return app;
};
