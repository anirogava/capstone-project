import { RouterFactory } from "../interfaces/general";
import express, { Response, Request } from "express";
import passport from "passport";
import bodyParser from "body-parser";
import { User, UserRole } from "../models/user.model";
import { Feedback } from "../models/feedback.model";
import permissions from "../middleware/checkPermission";
import { body, query, validationResult } from "express-validator";
import {CacheService} from "../services/CacheService";

const urlencodedParser = bodyParser.urlencoded({ extended: false });

export const makeFeedback: RouterFactory = () => {
  const router = express.Router();

  router.post(
    "",
    urlencodedParser,
    passport.authenticate("jwt", { session: false }),
    permissions(["Admin", "User"]),
    [
      body("fromUser").isInt(),
      body("companyName").isString().isLength({ max: 255 }),
      body("toUser").isInt(),
      body("context").isString(),
    ],
    async (req: Request, res: Response) => {
      const user = req.user as User;
      req.log.info('Log message from the route handler');

      const { fromUser, toUser } = req.body;

      if (fromUser === toUser) {
        return res.status(400).json({
          message: "You cannot create feedback for yourself.",
        });
      }
      if (fromUser != user.id) {
        return res.status(400).json({
          message: "You cannot create feedback for someone else.",
        });
      }

      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      try {
        const newFeedback = await Feedback.create({
          from_user: req.body.fromUser,
          company_name: req.body.companyName,
          to_user: req.body.toUser,
          content: req.body.context,
        });
        await CacheService(user.id);

        return res.status(201).json({
          id: newFeedback.id,
          fromUser: newFeedback.from_user,
          companyName: newFeedback.company_name,
          toUser: newFeedback.to_user,
          context: newFeedback.content,
        });
      } catch (error) {
        console.error(error);
        return res.status(500).json({
          error: "Something went wrong on the server.",
        });
      }
    },
  );

  router.get(
    "/",
    urlencodedParser,
    passport.authenticate("jwt", { session: false }),
    permissions(["Admin"]),
    query("pageSize").notEmpty(),
    query("page").notEmpty(),
    async (req: Request, res: Response) => {
      const pageSize = Number(req.query.pageSize) || 10;
      const page = Number(req.query.page) || 1;
      const user = req.user as User;
      req.log.info('Log message from the route handler');

      try {
        const { rows: feedbackEntries, count: totalCount } =
          await Feedback.findAndCountAll({
            limit: pageSize,
            offset: (page - 1) * pageSize,
          });

        const formattedFeedbackEntries = feedbackEntries.map((feedback) => ({
          id: feedback.id,
          fromUser: feedback.from_user,
          companyName: feedback.company_name,
          toUser: feedback.to_user,
          context: feedback.content,
        }));

        res.setHeader("X-total-count", totalCount);
        await CacheService(user.id);

        return res.status(200).json(formattedFeedbackEntries);
      } catch (error) {
        console.error(error);
        return res.status(500).json({
          error: "Something went wrong on the server.",
        });
      }
    },
  );

  router.get("/:id", urlencodedParser, async (req, res) => {
    req.log.info('Log message from the route handler');
    try {
      const feedbackId = parseInt(req.params.id, 10);

      if (isNaN(feedbackId)) {
        return res.status(400).json({
          error: "Invalid ID provided. The ID should be a number.",
        });
      }

      const feedback = await Feedback.findByPk(feedbackId);

      if (!feedback) {
        return res.status(404).json({
          message: "Feedback with the provided ID does not exist.",
        });
      }

      return res.status(200).json({
        id: feedback.id,
        fromUser: feedback.from_user,
        companyName: feedback.company_name,
        toUser: feedback.to_user,
        context: feedback.content,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        error: "Something went wrong on the server.",
      });
    }
  });

  router.put(
    "/:id",
    urlencodedParser,
    passport.authenticate("jwt", { session: false }),
    [
      body("fromUser").isInt(),
      body("toUser").isInt(),
      body("context").isString().isLength({ max: 256 }),
      body("companyName").isString().isLength({ max: 256 }),
    ],
    async (req: Request, res: Response) => {
      const user = req.user as User;
      const feedbackId = parseInt(req.params.id, 10);
      req.log.info('Log message from the route handler');

      if (isNaN(feedbackId)) {
        return res.status(400).json({
          error: "Invalid ID provided. The ID should be a number.",
        });
      }

      try {
        const validationErrors = validationResult(req);
        if (!validationErrors.isEmpty()) {
          return res.status(400).json({ errors: validationErrors.array() });
        }

        const feedback = await Feedback.findByPk(feedbackId);

        if (!feedback) {
          return res.status(404).json({
            message: "Feedback not found.",
          });
        }

        if (
          user.role === UserRole.Admin ||
          feedback.from_user === user.id
        ) {
          const { fromUser, companyName, toUser, context } = req.body;

          await feedback.update({
            from_user: fromUser,
            company_name: companyName,
            to_user: toUser,
            content: context,
          });

          const updatedFeedback = await Feedback.findByPk(feedbackId);
          await CacheService(user.id);

          return res.status(200).json({
            id: updatedFeedback.id,
            fromUser: updatedFeedback.from_user,
            companyName: updatedFeedback.company_name,
            toUser: updatedFeedback.to_user,
            context: updatedFeedback.content,
          });
        } else {
          return res.status(403).json({
            message: "No Permission.",
          });
        }
      } catch (error) {
        console.error(error);
        return res.status(500).json({
          error: "Something went wrong.",
        });
      }
    },
  );

  router.delete(
    "/:id",
    urlencodedParser,
    passport.authenticate("jwt", { session: false }),
    async (req, res) => {
      const user = req.user as User;
      const feedbackId = parseInt(req.params.id, 10);
      req.log.info('Log message from the route handler');

      if (isNaN(feedbackId)) {
        return res.status(400).json({
          error: "Invalid ID provided. The ID should be a number.",
        });
      }

      try {
        const feedback = await Feedback.findByPk(feedbackId);

        if (!feedback) {
          return res.status(404).json({
            message: "Feedback not found.",
          });
        }
        if (
          user.role === UserRole.Admin ||
          feedback.from_user === user.id
        ) {
          await feedback.destroy();
          await CacheService(user.id);
          return res.status(204).send();
        } else {
          return res.status(403).json({
            message:
              "Permission denied. You are not authorized to delete this feedback.",
          });
        }
      } catch (error) {
        console.error(error);
        return res.status(500).json({
          error: "Something went wrong on the server.",
        });
      }
    },
  );

  return router;
};
