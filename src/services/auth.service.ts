import { Request, Response } from "express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { validationResult } from "express-validator";
import { User, UserRole } from "../models/user.model";

const secretKey = process.env.JWT_SECRET || "your-default-secret-key";

export class AuthService {
  static async register(req: Request, res: Response) {
    req.log.info('Log message from the route handler');
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { firstName, lastName, title, summary, email, password } = req.body;

    try {
      if (!password) {
        return res.status(400).json({ error: "Password is required" });
      }

      const image = req.file ? req.file.filename : null;
      const hashedPassword = await bcrypt.hash(password, 10);
      const role = UserRole.User;

      const user = await User.create({
        firstName,
        lastName,
        title,
        summary,
        email,
        password: hashedPassword,
        image,
        role,
      });
      const token = jwt.sign({ id: user.id }, secretKey, {
        expiresIn: "1h",
      });
      const userResponse = {
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        image: user.image,
        role: user.role,
      };
      res.status(201).json({
        user: userResponse,
        token,
      });
    } catch (error) {
      console.error(error);
      res.status(400).json({ error: "Registration failed" });
    }
  }

  static async login(req: Request, res: Response) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { email, password } = req.body;

    const user = await User.findOne({ where: { email } });

    if (!user) {
      return res.status(400).json({ error: "Invalid credentials" });
    }

    const passwordMatch = await bcrypt.compare(password, user.password);

    if (!passwordMatch) {
      return res.status(400).json({ error: "Invalid credentials" });
    }

    const token = jwt.sign({ id: user.id }, secretKey, {
      expiresIn: "1h",
    });

    const userResponse = {
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      image: user.image,
      role: user.role,
    };

    res.status(200).json({
      user: userResponse,
      token,
    });
  }
}
