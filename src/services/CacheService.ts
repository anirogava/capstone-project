import { redisClient } from "../redis_client";

const CacheService = (userId: number): void => {
  const cacheKey = `cv:${userId}`;
  redisClient.del(cacheKey, (err: Error | null, numDeleted: number) => {
    if (err) {
      console.error(err);
    } else {
      console.log(`Cache cleared for key: ${cacheKey}`);
    }
  });
};

export { CacheService };
