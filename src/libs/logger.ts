import pino from "pino";

export const Logger = (requestId: string) => {
  return pino({
    prettyPrint: process.env.NODE_ENV === "development",
    base: {
      requestId,
    },
  });
};
