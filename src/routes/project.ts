import { RouterFactory } from "../interfaces/general";
import express, { Response, Request } from "express";
import passport from "passport";
import bodyParser from "body-parser";
import { body, query } from "express-validator";
import multer from "multer";

import { User, UserRole } from "../models/user.model";
import { Project } from "../models/project.model";
import permissions from "../middleware/checkPermission";
import { storageForProject } from "../libs/utils";
import {CacheService} from "../services/CacheService";

const upload = multer({ storage: storageForProject });
const urlencodedParser = bodyParser.urlencoded({ extended: false });

export const makeProject: RouterFactory = () => {
  const router = express.Router();

  router.post(
    "",
    urlencodedParser,
    upload.single("image"),
    passport.authenticate("jwt", { session: false }),
    permissions(["Admin", "User"]),
    [
      body("user_id").isInt(),
      body("image").isString().isLength({ max: 256 }),
      body("description").isString().isLength({ max: 256 }),
    ],
    async (req: Request, res: Response) => {
      req.log.info('Log message from the route handler');
      const { userId, description } = req.body;
      const user = req.user as User;

      try {
        const image = req.file ? req.file.filename : null;

        const project = await Project.create({
          user_id: userId,
          image: image,
          description: description,
        });
        await CacheService(user.id);

        res.status(201).json({
          id: project.id,
          userId: project.user_id,
          image: project.image,
          description: project.description,
        });
      } catch (error) {
        console.error(error);
        return res
          .status(500)
          .json({ error: "Something went wrong on the server." });
      }
    },
  );

  router.get(
    "",
    passport.authenticate("jwt", { session: false }),
    permissions(["Admin"]),
    query("pageSize").notEmpty(),
    query("page").notEmpty(),
    async (req: Request, res: Response) => {
      req.log.info('Log message from the route handler');
      const currentUser = req.user as User;
      if (currentUser.role !== UserRole.Admin) {
        return res.status(403).json({
          message:
            "Permission denied. You are not authorized to access this resource.",
        });
      }

      try {
        const pageSize = Number(req.query.pageSize);
        const page = Number(req.query.page);

        if (isNaN(pageSize) || isNaN(page)) {
          return res.status(400).json({
            message:
              "Invalid pageSize or page value. Please provide valid numbers.",
          });
        }

        const offset = (page - 1) * pageSize;

        const { rows: projects, count: totalCount } =
          await Project.findAndCountAll({
            limit: pageSize,
            offset: offset,
          });

        res.status(200).json({
          projects,
          totalCount,
        });
      } catch (error) {
        console.error(error);
        return res
          .status(500)
          .json({ error: "Something went wrong on the server." });
      }
    },
  );

  router.get("/:id", async (req: Request, res: Response) => {
    req.log.info('Log message from the route handler');
    try {
      const projectId = parseInt(req.params.id, 10);
      if (isNaN(projectId)) {
        return res.status(400).json({
          error: "Invalid ID provided. The ID should be a number.",
        });
      }

      const project = await Project.findByPk(projectId);

      if (!project) {
        return res.status(404).json({ message: "Project not found" });
      }

      res.status(200).json({
        id: project.id,
        userId: project.user_id,
        image: project.image,
        description: project.description,
      });
    } catch (error) {
      console.error(error);
      return res
        .status(500)
        .json({ error: "Something went wrong on the server." });
    }
  });

  router.put(
    "/:id",
    urlencodedParser,
    passport.authenticate("jwt", { session: false }),
    [
      body("user_id").isInt(),
      body("image").isString().isLength({ max: 256 }),
      body("description").isString().isLength({ max: 256 }),
    ],
    async (req: Request, res: Response) => {
      req.log.info('Log message from the route handler');
      const user = req.user as User;
      const projectId = req.params.id;

      try {
        const project = await Project.findByPk(projectId);

        if (!project) {
          return res.status(404).json({ message: "Project not found" });
        }
        if (
          user.role !== UserRole.Admin &&
          project.user_id !== user.id
        ) {
          return res.status(403).json({
            message: "You do not have permission to update this project.",
          });
        }

        const { userId, image, description } = req.body;

        project.user_id = userId;
        project.image = image;
        project.description = description;

        await project.save();
        await CacheService(user.id);

        res.status(200).json({
          id: project.id,
          userId: project.user_id,
          image: project.image,
          description: project.description,
        });
      } catch (error) {
        console.error(error);
        return res
          .status(500)
          .json({ error: "Something went wrong on the server." });
      }
    },
  );

  router.delete(
    "/:id",
    passport.authenticate("jwt", { session: false }),
    async (req: Request, res: Response) => {
      req.log.info('Log message from the route handler');
      const user = req.user as User;
      const projectId = parseInt(req.params.id, 10);
      if (isNaN(projectId)) {
        return res.status(400).json({
          error: "Invalid ID provided. The ID should be a number.",
        });
      }

      try {
        const project = await Project.findByPk(projectId);

        if (!project) {
          return res.status(404).json({ message: "Project not found" });
        }
        if (
          user.role !== UserRole.Admin &&
          project.user_id !== user.id
        ) {
          return res.status(403).json({
            message: "You do not have permission to delete this project.",
          });
        }

        await project.destroy();
        await CacheService(user.id);

        return res.sendStatus(204);
      } catch (error) {
        console.error(error);
        return res
          .status(500)
          .json({ error: "Something went wrong on the server." });
      }
    },
  );

  return router;
};
