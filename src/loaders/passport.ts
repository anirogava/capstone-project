import { Loader } from "../interfaces/general";
import { User } from "../models/user.model";

import passport from "passport";
import { Strategy, ExtractJwt } from "passport-jwt";

export const jwtSecret = "your-default-secret-key";

export const loadPassport: Loader = (app) => {
  const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: jwtSecret,
  };

  app.use(passport.initialize());
  passport.use(
    new Strategy(jwtOptions, async (payload, done) => {
      try {
        console.log(payload);
        const user = await User.findOne({ where: { id: payload.id } });
        if (!user) {
          return done(null, false);
        }
        return done(null, user);
      } catch (error) {
        return done(error, false);
      }
    }),
  );

  return passport;
};
