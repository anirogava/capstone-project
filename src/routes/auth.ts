import { RouterFactory } from "../interfaces/general";
import express from "express";
import bodyParser from "body-parser";
import multer from "multer";
import { body } from "express-validator";
import { AuthService } from "../services/auth.service";
import { storage } from "../libs/utils";

const upload = multer({ storage: storage });
const urlencodedParser = bodyParser.urlencoded({ extended: false });

export const makeAuthRouter: RouterFactory = () => {
  const router = express.Router();

  router.post(
    "/register",
    urlencodedParser,
    upload.single("image"),
    [
      body("firstName").isString().notEmpty(),
      body("lastName").isString().notEmpty(),
      body("title").isString().notEmpty(),
      body("summary").isString().notEmpty(),
      body("email").isEmail().notEmpty(),
      body("password").isLength({ min: 6 }),
    ],
    AuthService.register,
  );

  router.post(
    "/login",
    urlencodedParser,
    [
      body("email").isEmail().withMessage("Invalid email format"),
      body("password")
        .isLength({ min: 6 })
        .withMessage("Password must be at least 6 characters"),
    ],
    AuthService.login,
  );

  return router;
};
