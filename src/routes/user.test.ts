import request from 'supertest';
import { loadApp } from '../loaders/app';


const Admintoken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjk4MTYzNTUzLCJleHAiOjE2OTgxNjcxNTN9.b0ORmvZDx079yrxznzpEk9B5RjkU9ahwscgckmBfPrY"

describe('User Routes', () => {
    describe('POST /users', () => {
        it('should register a new user', async () => {
            const app = await loadApp();
            const response = await request(app)
                .post('/api/users')
                .set('Authorization', `Bearer ${Admintoken}`)
                .field('firstName', 'John')
                .field('lastName', 'Doe')
                .field('title', 'Software Engineer')
                .field('summary', 'A software engineer')
                .field('email', 'testkjnsakjdnaksjndasdhbhj@dsdsd.com')
                .field('password', 'securepassword')
                .field('role', 'User')
                .attach('image', 'uploads/image-1695896151126-694614170.jpg');


            expect(response.status).toBe(201);
            expect(response.body).toHaveProperty('id');
            expect(response.body).toHaveProperty('firstName', 'John');
        });

        it('should fail to register with invalid data', async () => {
            const app = await loadApp();

            const response = await request(app)
                .post('/api/users')
                .set('Authorization', `Bearer ${Admintoken}`)
                .send({
                });

            expect(response.status).toBe(400);
        });

    });

    describe('GET /users', () => {
        it('should retrieve a list of users with pagination', async () => {
            const app = await loadApp();

            const response = await request(app)
                .get('/api/users')
                .set('Authorization', `Bearer ${Admintoken}`)
                .query({ pageSize: "10", page: "1" });

            expect(response.status).toBe(201);
            expect(response.body).toHaveProperty('users');
        });

        it('should fail with invalid pagination parameters', async () => {
            const app = await loadApp();

            const response = await request(app)
                .get('/api/users')
                .set('Authorization', `Bearer ${Admintoken}`)
                .query({ pageSize: 'invalid', page: 'invalid' });

            expect(response.status).toBe(400);
        });

    });

    describe('GET /api/users/:id', () => {
        it('should retrieve a user by ID', async () => {
            const userId = 1;
            const app = await loadApp();

            const response = await request(app)
                .get(`/api/users/${userId}`)
                .set('Authorization', `Bearer ${Admintoken}`);

            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty('id', userId);
        });

        it('should fail to retrieve with an invalid user ID', async () => {
            const userId = 'invalid';
            const app = await loadApp();

            const response = await request(app)
                .get(`/api/users/${userId}`)
                .set('Authorization', `Bearer ${Admintoken}`);

            expect(response.status).toBe(400);
        });

    });

    describe('PUT /users/:id', () => {
        it('should update a user profile', async () => {
            const userId = 1;
            const app = await loadApp();

            const response = await request(app)
                .put(`/api/users/${userId}`)
                .set('Authorization', `Bearer ${Admintoken}`)
                .field('firstName', 'Updated John')
                .field('lastName', 'Updated Doe')
                .field('title', 'Updated Software Engineer')
                .field('role', 'Admin')
                .field('summary', 'arvici es ra oxeria daapdeitebulia cexavikebis mier')
                .field('email', 'cexaviki1@mail.ru')
                .field('image', 'image-1695896151126-694614170.jpg');

            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty('id', userId);
        });

        it('should fail to update with an invalid user ID', async () => {
            const app = await loadApp();

            const response = await request(app)
                .put(`/api/users/${232323}`)
                .set('Authorization', `Bearer ${Admintoken}`)
                .field('firstName', 'Updated John')
                .field('lastName', 'Updated Doe')
                .field('image', 'image-1695896151126-694614170.jpg');

            expect(response.status).toBe(404);
        });

    });

    describe('DELETE /users/:id', () => {
        it('should delete a user account', async () => {
            const userId = 176;
            const app = await loadApp();

            const response = await request(app)
                .delete(`/api/users/${userId}`)
                .set('Authorization', `Bearer ${Admintoken}`);

            expect(response.status).toBe(204);
        });

        it('should fail to delete with an invalid user ID', async () => {
            const userId = 'invalid';
            const app = await loadApp();

            const response = await request(app)
                .delete(`/users/${userId}`)
                .set('Authorization', `Bearer ${Admintoken}`);

            expect(response.status).toBe(404);
        });
    });
});

