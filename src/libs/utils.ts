import multer from "multer";

export const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads/");
  },
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    const ext = file.originalname.split(".").pop();
    const filename = `${file.fieldname}-${uniqueSuffix}.${ext}`;
    cb(null, filename);
  },
});

export const storageForProject = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "projectimages/");
  },
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    const ext = file.originalname.split(".").pop(); // Get the file extension
    const filename = `${file.fieldname}-${uniqueSuffix}.${ext}`;
    cb(null, filename);
  },
});
